import { Directive, ElementRef, HostBinding, HostListener } from "@angular/core";

/** opens and closes dropdown menus, even when clicked elsewhere in the DOM */
@Directive({
    selector: '[appDropdown]'
})
export class DropDownDirective {
    @HostBinding('class.open') open = false;

    constructor(private elRef: ElementRef) {}

    @HostListener('document:click',['$event']) toggleOpen(event: Event) {
        this.open = this.elRef.nativeElement.contains(event.target) ? !this.open : false;
    }
}