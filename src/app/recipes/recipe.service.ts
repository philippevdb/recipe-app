import { Injectable } from "@angular/core";

import { Recipe } from "./recipe-list/recipe.model";
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();
  private recipes: Recipe[] = [];
  
  // private recipes: Recipe[] = [
  //   new Recipe('A Test Recipe',
  //   'This is simply a test',
  //   'https://www.tasteofhome.com/wp-content/uploads/2018/01/exps7986_WD949516D51.jpg',
  //   [
  //     new Ingredient('Beef',1),
  //     new Ingredient('Broccoli', 5)
  //   ]),
  //   new Recipe('A Test Recipe2',
  //   'This is simply a test',
  //   'https://www.tasteofhome.com/wp-content/uploads/2018/01/exps7986_WD949516D51.jpg',
  //   [
  //     new Ingredient('Chicken wing', 1),
  //     new Ingredient('Salad', 20)
  //   ])
  // ];
  
  constructor(private shoppingListService: ShoppingListService) {}
  
    setRecipes(recipes: Recipe[]) {
      this.recipes = recipes;
      this.recipesChanged.next(this.recipes.slice());
    }
  
  getRecipe(id: number): Recipe {
    return this.recipes.slice()[id];
  }

  getRecipes(): Recipe[] {
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }
}