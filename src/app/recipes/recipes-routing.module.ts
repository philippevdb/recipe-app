import { NgModule } from "@angular/core";
import { RouterModule, Routes, mapToCanActivate } from "@angular/router";

import { AuthGuardService } from "../auth/auth-guard.service";
import { RecipesComponent } from "./recipes.component";
import { RecipeStartComponent } from "./recipe-start/recipe-start.component";
import { RecipeEditComponent } from "./recipe-edit/recipe-edit.component";
import { RecipeDetailComponent } from "./recipe-detail/recipe-detail.component";
import { RecipesResolverService } from "./recipes-resolver.service";

const routes: Routes = [
    {path: '', component: RecipesComponent, canActivate: mapToCanActivate([AuthGuardService]), children: [
        {path: '', component: RecipeStartComponent},
        {path: 'new', component: RecipeEditComponent},
        {path: ':id', component: RecipeDetailComponent,  resolve: {recipes: RecipesResolverService.fetchRecipes}}, // be careful, if new after :id, angular will think 'new' is an id
        {path: ':id/edit', component: RecipeEditComponent,  resolve: {recipes: RecipesResolverService.fetchRecipes}},
    ]},
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule
    ]
})
export class RecipesRoutingModule {

}