import { Injectable, inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';
import { Recipe } from './recipe-list/recipe.model';
import { DataStorageService } from '../shared/data-storage.service';
import { RecipeService } from './recipe.service';

@Injectable({
  providedIn: 'root'
})
export class RecipesResolverService {

  constructor() { }
  
  static fetchRecipes: ResolveFn<Recipe[]> = 
  (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    const recipes = inject(RecipeService).getRecipes();
    if (recipes.length === 0) {
      return inject(DataStorageService).fetchRecipes();
    } else {
      return recipes;
    }
  };
}
